<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BlogPost;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/frontend/index", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));
    }

    /**
     * @Route("/frontend/forgot_password", name="forgot_password")
     */
    public function forgotPasswordAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('frontend/forgotPassword.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));
    }

    /**
     * @Route("/frontend/reservation", name="reservation")
     */
    public function reservationAction(Request $request)
    {
        $booking = new BlogPost();
        $form = $this->createForm('AppBundle\Form\PostType', $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($booking);
            $em->flush();

            $this->addFlash(
                'success',
                'Booking created successfully'
            );

            $message = (new \Swift_Message('Hello Email'))
                ->setFrom('send@example.com')
                ->setTo($booking->getEmail())
                ->setBody("<h1>Your Booking details</h1>" .
                "<ul>".
                    "<li>Date : " . $booking->getDate()->format('Y-m-d')."</li>".
                    "<li>time : " . $booking->getTime()->format('H:i:s')."</li>".
                    "<li>session : " . $booking->getSession()."</li>".
                    "<li>guests : " . $booking->getGuests()."</li>".
                    "<li>title : " . $booking->getTitle()."</li>".
                    "<li>firstName : " . $booking->getFirstName()."</li>".
                    "<li>lastName : " . $booking->getLastName()."</li>".
                    "<li>telephone : " . $booking->getTelephone()."</li>".
                    "<li>email : " . $booking->getEmail()."</li>".
                    "<li>note : " . $booking->getNote()."</li>".
                "</ul>")
                /*
                 * If you also want to include a plaintext version of the message
                ->addPart(
                    $this->renderView(
                        'Emails/registration.txt.twig',
                        array('name' => $name)
                    ),
                    'text/plain'
                )
                */
            ;

            $this->get('mailer')->send($message);

//            return $this->redirectToRoute('mf_school_show', array('id' => $school->getId()));
        }

        // replace this example code with whatever you need
        return $this->render('frontend/reservation.html.twig', array(
            'booking' => $booking,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/frontend/menu", name="menu")
     */
    public function menuAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('frontend/menu.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));
    }

    /**
     * @Route("/frontend/contact", name="contact")
     */
    public function contactAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('frontend/contact.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));
    }
}
