<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('date', 'date')
            ->add('time', 'time')
            ->add('session', ChoiceType::class, array(
                'choices'  => array(
                    'Select' => "Select",
                    'Breakfast' => "Breakfast",
                    'Lunch' => "Lunch",
                    'Dinner' => "Dinner",
                ),
                // *this line is important*
                'choices_as_values' => true,
            ))
            ->add('guests', ChoiceType::class, array(
                'choices'  => array(
                    'Select' => "Select",
                    '01' => "01",
                    '02' => "02",
                    '03' => "03",
                    '04' => "04",
                    '05' => "05",
                    '06' => "06",
                    '07' => "07",
                    '08' => "08",
                    '09' => "09",
                    '10' => "10",
                ),
                // *this line is important*
                'choices_as_values' => true,
            ))
            ->add('title', 'text')
            ->add('firstName', 'text')
            ->add('lastName', 'text')
            ->add('telephone', 'text')
            ->add('email', 'text')
            ->add('note', 'textarea');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\BlogPost'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_blogpost_post';
    }


}
